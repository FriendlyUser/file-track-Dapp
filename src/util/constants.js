const NETWORKS = { 
'1': 'Main Net',
'2': 'Deprecated Morden',
'3': 'Ropsten',
'4': 'Rinkeby',
'42': 'Kovan',
} 

const IPFSURL = "https://gateway.ipfs.io/ipfs/"
export { NETWORKS, IPFSURL};
// https://gateway.ipfs.io/ipfs/QmZb7crH2YYqwvq5d2pCjZxAovzqXkhWwnEE993UM4jikk